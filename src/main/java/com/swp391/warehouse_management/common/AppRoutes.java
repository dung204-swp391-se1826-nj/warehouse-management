package com.swp391.warehouse_management.common;

import java.util.List;

public class AppRoutes {

  public static final String DASHBOARD = "/";
  public static final String ADMIN = "/admin";

  public static final String LOGIN = "/login";
  public static final String LOGOUT = "/logout";

  public static final List<String> publicRoutes = List.of(DASHBOARD, LOGIN);

  public static final List<String> privateRoutes = List.of(LOGOUT, ADMIN);

  public static boolean isPublicRoute(String route) {
    return publicRoutes.contains(route);
  }

  public static boolean isPrivateRoute(String route) {
    return privateRoutes.contains(route);
  }
}
