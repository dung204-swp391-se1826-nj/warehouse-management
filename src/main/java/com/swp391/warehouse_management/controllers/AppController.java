package com.swp391.warehouse_management.controllers;

import com.swp391.warehouse_management.common.AppRoutes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {

  @GetMapping(AppRoutes.DASHBOARD)
  public String dashboard() {
    return "index";
  }
}
